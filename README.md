# arca-challenge

## Iniciando con vagrant + virtualbox

Requerimientos:
- [Vagrant](https://www.vagrantup.com/downloads.html)
- [Virtualbox](https://www.virtualbox.org/wiki/Downloads)

Para hacer el provisionamiento automaticamente posicionarse dentro la carpeta a nivel root y escribir:
```bash
vagrant up
```

Si todo el provisionamiento sale bien, ejecutar:
```bash
vagrant ssh
```

Nos conectará a la ssh e ir a la ruta
```bash
cd /vagrant/src
flask run --host=0.0.0.0 --port=5000
```
Nota: **De manera automática se crearán dos archivos .env y .flaskenv para iniciar el proyecto. 
Si por alguna razón no se crea, ejecutar**

```bash
vagrant provision
```
Test endpoints
```bash
curl -X POST http://127.0.0.1:5001/v1.0/mutations -H "Content-Type: application/json" -d '{"dna": ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]}'
curl -X GET http://127.0.0.1:5001/v1.0/stats
```

## Iniciando el proyecto con docker
Requerimientos:
- Docker v1.13.0+
- docker-compose

Para ejecutar con docker, dentro de la carpeta src debe existir el archivo .env y .flaskenv que debe contener lo siguiente:
- .env
>SQLALCHEMY_DATABASE_URI=mysql+pymysql://root:passworddb@mariadb:3306/arca

- .flaskenv
>FLASK_APP=/home/python/app/src/manage.py\
>FLASK_ENV=development

Posicionarse a nivel del archivo docker-compose.yml y ejecutar:
```bash
docker-compose up -d --build
```
Test endpoints
```bash
curl -X POST http://127.0.0.1:5000/v1.0/mutations -H "Content-Type: application/json" -d '{"dna": ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]}'
curl -X GET http://127.0.0.1:5000/v1.0/stats
```