FROM python:3.8.2-slim

ENV APP_USER=python
RUN groupadd -r ${APP_USER} && useradd --no-log-init -r -g ${APP_USER} ${APP_USER}

RUN apt-get update && apt-get install -qy build-essential software-properties-common python3-pip python3-dev

WORKDIR /home/python/app
ADD . ./

RUN pip3 install -r ./requirements.txt --no-cache-dir && apt-get purge -y && apt-get autoremove -y

USER ${APP_USER}:${APP_USER}
