from typing import List
from copy import deepcopy


class Dna:
    def __init__(self, sequences: List[str]):
        self.sequences = sequences

    def __ensure_has_mutation(self, chains: List[str]) -> bool:
        chain_to_compare = chains[0]
        chains.remove(chain_to_compare)

        counter = 0
        has_mutation = False
        for chain in chains:
            if chain == chain_to_compare:
                counter = counter + 1
            else:
                return self.__ensure_has_mutation(chains)

            has_mutation = counter > 2
            if has_mutation:
                break
        return has_mutation

    def __vertical_sequences(self, chains, index: int = 0, start_at: int = 0,
                             vertical_elements: list = None) -> list:
        if index == len(chains):
            return vertical_elements
        item = chains[index][start_at]
        vertical_elements.append(item)
        return self.__vertical_sequences(chains, (index + 1), start_at, vertical_elements)

    def __get_horizontal_mutations(self, sequences: List[list]) -> int:
        mutations_found = 0
        for parent in sequences:
            has_horizontal_mutation = self.__ensure_has_mutation(parent)
            if has_horizontal_mutation:
                mutations_found = mutations_found + 1
        return mutations_found

    def __get_vertical_mutations(self, sequences: List[list]) -> int:
        mutations_found = 0
        for index in range(6):
            vertical = self.__vertical_sequences(
                sequences,
                index=0,
                start_at=index,
                vertical_elements=list()
            )

            has_vertical_mutation = self.__ensure_has_mutation(vertical)
            if has_vertical_mutation:
                mutations_found = mutations_found + 1
        return mutations_found

    def has_mutation(self):
        parents = list()
        for sequence in self.sequences:
            children = list()
            for item in list(sequence):
                children.append(item)
            parents.append(children)

        has_mutation = self.__get_horizontal_mutations(deepcopy(parents))
        has_mutation = has_mutation + self.__get_vertical_mutations(deepcopy(parents))
        return has_mutation >= 2
