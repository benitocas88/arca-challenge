from flask import Blueprint
from flask_restful import Api

from .mutations import Mutations, Stats


blueprint = Blueprint("api", __name__, url_prefix="/v1.0")
api = Api(blueprint)

"""
TODO: Add all resources
"""
api.add_resource(Mutations, "/mutations")
api.add_resource(Stats, "/stats")
