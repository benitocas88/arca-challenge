from flask_restful import Resource
from http import HTTPStatus
from webargs.flaskparser import use_args
from models import Mutation
from schemas import MutationSchema


class Mutations(Resource):
    @use_args(MutationSchema)
    def post(self, args):
        mutation = Mutation().dna(args.get("dna"))
        status_code = HTTPStatus.OK if mutation.has_mutation else HTTPStatus.FORBIDDEN
        return mutation.has_mutation, status_code


class Stats(Resource):
    @staticmethod
    def get():
        stats = Mutation.stats()
        return stats
