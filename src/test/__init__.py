import unittest
from models import Mutation


class TestMutationMethods(unittest.TestCase):
    def test_create_mutation(self):
        mutation = Mutation()
        has_mutation = mutation.dna(["C", "E", "F", "C", "E", "F"])
        self.assertIsInstance(has_mutation, Mutation)


if __name__ == "__main__":
    unittest.main()
