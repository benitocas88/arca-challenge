import os
from factory import factory


"""
temporal trick to wait mariadb container is ready to connect
better options => https://docs.docker.com/compose/startup-order/
"""
if os.getenv("FLASK_ENV") == "development":
    import time
    time.sleep(1.5)

app = factory.flask

