from marshmallow import Schema, fields, validate


class MutationSchema(Schema):
    class Meta:
        ordered = True
        fields = ("dna",)
        load_only = ("dna",)

    dna = fields.List(
        required=True,
        validate=validate.Length(equal=6),
        cls_or_instance=fields.String(
            required=True,
            validate=[
                validate.Length(equal=6),
                validate.ContainsOnly(["A", "T", "C", "G"])
            ]
        )
    )
