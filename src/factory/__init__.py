import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from .config import app_config


class Factory:
    flask = None

    def __init__(self):
        self.environment = os.getenv("FLASK_ENV", "development")

        self.flask = Flask(__name__)
        self.db = SQLAlchemy()

    def init_app(self):
        self.flask.url_map.strict_slashes = False
        self.flask.config.from_object(app_config.get(self.environment))

        self.db.init_app(self.flask)
        Migrate(
            app=self.flask,
            db=self.db,
            compare_type=True
        )

        from api import blueprint as api
        self.flask.register_blueprint(api)


factory = Factory()
factory.init_app()
