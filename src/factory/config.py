import os
from dotenv import load_dotenv

load_dotenv()


class Config:
    DEBUG = False
    SECRET_KEY = os.getenv("SECRET_KEY")

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = os.getenv("SQLALCHEMY_DATABASE_URI")


class Development(Config):
    DEBUG = True


class Production(Config):
    pass


app_config = dict(
    development=Development,
    production=Production
)
