from sqlalchemy.exc import IntegrityError
from datetime import datetime
from factory import factory
from dna import Dna


db = factory.db


class Mutation(db.Model):
    __tablename__ = "mutation"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow, nullable=False)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow, nullable=False)
    has_mutation = db.Column(db.Boolean, default=False, nullable=False)
    sequences = db.Column("sequence_chain", db.JSON, unique=True, nullable=False)

    def dna(self, sequences):
        mutation_exists = self.query.filter_by(sequences=sequences).first()
        if mutation_exists:
            return mutation_exists

        dna_mutation = Dna(sequences)
        self.has_mutation = dna_mutation.has_mutation()
        self.sequences = sequences

        try:
            db.session.add(self)
            db.session.commit()
            return self
        except IntegrityError as error:
            db.session.rollback()
            raise error

    @classmethod
    def stats(cls):
        qs = cls.query.filter
        count_mutations = qs(cls.has_mutation.is_(True)).count()
        count_no_mutations = qs(cls.has_mutation.is_(False)).count()
        ratio = 0
        if count_no_mutations > 0:
            ratio = count_mutations / count_no_mutations

        return {
            "count_mutations": count_mutations,
            "count_no_mutations": count_no_mutations,
            "ratio": float("%.1f" % ratio)
        }
